//: Playground - noun: a place where people can play

// Zadanie 11

import Foundation

func isPrime(num: Int) -> Bool {
    if num < 2 {
        return false
    }
    
    for i in 2..<num {
        if num % i == 0 {
            return false
        }
    }
    
    return true
}

func randomArray(len: Int) -> [Int] {
    var results = [Int]()
    
    for _ in 0..<len {
        results.append(Int(arc4random_uniform(100)))
    }
    
    return results.filter(isPrime)
}

randomArray(100)

// Zadanie 12


func randomArray2( n: Int) -> String {
    var results = [Int]()
    var min=10
    var max=10
    var k: Int
    
    for _ in 0..<n {
        results.append(Int(arc4random_uniform(UInt32(n))))
}
    for k=0; k < results.count;k++ {
        if results[k]<min {
            min=results[k]
        }}
    for k=0; k < results.count;k++ {
    if (max>results[k]) && (results[k]>min) {
         max=results[k]
        }}
    return "Minimal #1=\(min) and Minimal #2=\(max)"
}
randomArray2(7)

//Zadanie 13

func diagonals (n:Int) -> String{
    var sumFirst = 0
    var sumSecond = 0
    
    var multArray = Array(count: n, repeatedValue: Array(count: n, repeatedValue: 0))
            print(multArray) //create 2d array NxN with Initialisation
    
    for indexI in multArray.indices {
        for indexK in multArray.indices {
            multArray[indexI][indexK] = Int(arc4random_uniform(UInt32(n))) // fill array elements randomly
            if indexI==indexK { //check element on the main diagonal
                 sumFirst += multArray[indexI][indexK]  // do sum
            }
            if (indexI+indexK + 1) == n  { //check element on secondary diagonal
                sumSecond += multArray[indexI][indexK] // do sum
            }
        }
    }
print(multArray)
    return "Sum elemets on main diagonal =\(sumFirst) and second =\(sumSecond)"
}

diagonals(3)


//Zadanie 14

func highDifference ( n: Int) ->String{
    var a = [Int]()
    var min = n
    var max = 0
    for i in 0..<n {
        a.append(Int(arc4random_uniform(UInt32(20))))
            if a[i] < min {
                min = a[i]
            }
            if a[i] > max {
                max = a[i]
            }
       
    }
    print(min)
    print(max)
    print(a)
    return "Maximum difference =\(max-min)"
}
    
highDifference(15)

func highDifferenceV ( n: Int) ->String{
    var a = [Int]() //array
    var dif = 0     // max difference
    var k = 0
  
    
    for _ in 0..<n {
        a.append(Int(arc4random_uniform(UInt32(26)))-5) // fill array
      
        
    }
    while k < a.count {  //search the greatest difference
        for i in 0..<n {
            if a[k] - a[i] > dif {
                dif = a[k] - a[i]
            }
        }
        k++
    }
    print(a)
    return "Maximum difference =\(dif)"
}

highDifferenceV(75)

func nod (var a:Int, b:Int) ->Int {
    while a>b {
        a = a-b
    }
    return abs(a)
}

nod(25, b:5)


func nodChisel (let a:Int, b:Int) ->Int{
    if b==0 {
        return a
    }
    else {
        return nodChisel(b, b: a%b)
        
    }
}
nodChisel(25, b: 5)
